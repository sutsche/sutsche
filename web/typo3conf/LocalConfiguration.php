<?php
return [
    'BE' => [
        'debug' => false,
        'disable_exec_function' => 0,
        'installToolPassword' => '108f221cf2e87d54493a793f1ec8bb5c',
        'loginSecurityLevel' => 'normal',
        'passwordHashing' => [
            'className' => 'TYPO3\\CMS\\Core\\Crypto\\PasswordHashing\\BcryptPasswordHash',
            'options' => [],
        ],
        'versionNumberInFilename' => '0',
    ],
    'DB' => [
        'Connections' => [
            'Default' => [
                'charset' => 'utf8',
                'dbname' => 'db11027778-sut',
                'driver' => 'mysqli',
                'host' => 'wp297',
                'password' => '81MTK!rtfm0815',
                'port' => 3306,
                'user' => 'db11027778-sut',
            ],
        ],
        'extTablesDefinitionScript' => 'extTables.php',
    ],
    'EXT' => [
        'extConf' => [
            'backend' => 'a:6:{s:9:"loginLogo";s:0:"";s:19:"loginHighlightColor";s:0:"";s:20:"loginBackgroundImage";s:0:"";s:13:"loginFootnote";s:0:"";s:11:"backendLogo";s:0:"";s:14:"backendFavicon";s:0:"";}',
            'extensionmanager' => 'a:2:{s:21:"automaticInstallation";s:1:"1";s:11:"offlineMode";s:1:"0";}',
            'gridelements' => 'a:3:{s:20:"additionalStylesheet";s:0:"";s:19:"nestingInListModule";s:1:"1";s:26:"overlayShortcutTranslation";s:1:"0";}',
            'scheduler' => 'a:2:{s:11:"maxLifetime";s:4:"1440";s:15:"showSampleTasks";s:1:"1";}',
        ],
    ],
    'EXTCONF' => [
        'lang' => [
            'availableLanguages' => [],
        ],
    ],
    'EXTENSIONS' => [
        'backend' => [
            'backendFavicon' => '',
            'backendLogo' => '',
            'loginBackgroundImage' => '',
            'loginFootnote' => '',
            'loginHighlightColor' => '',
            'loginLogo' => '',
        ],
        'extensionmanager' => [
            'automaticInstallation' => '1',
            'offlineMode' => '0',
        ],
        'gridelements' => [
            'additionalStylesheet' => '',
            'nestingInListModule' => '1',
            'overlayShortcutTranslation' => '0',
        ],
        'scheduler' => [
            'maxLifetime' => '1440',
            'showSampleTasks' => '1',
        ],
    ],
    'FE' => [
        'debug' => false,
        'loginSecurityLevel' => 'normal',
        'passwordHashing' => [
            'className' => 'TYPO3\\CMS\\Core\\Crypto\\PasswordHashing\\BcryptPasswordHash',
            'options' => [],
        ],
    ],
    'GFX' => [
        'gdlib_png' => 0,
        'imagefile_ext' => 'gif,jpg,jpeg,tif,tiff,bmp,pcx,tga,png,pdf,ai, svg',
        'jpg_quality' => '90',
        'processor' => 'GraphicsMagick',
        'processor_effects' => false,
        'thumbnails_png' => '1',
    ],
    'HTTP' => [],
    'INSTALL' => [],
    'MAIL' => [
        'transport' => 'mail',
        'transport_sendmail_command' => '',
        'transport_smtp_encrypt' => '',
        'transport_smtp_password' => '',
        'transport_smtp_server' => 'localhost:25',
        'transport_smtp_username' => '',
    ],
    'SYS' => [
        'devIPmask' => '',
        'displayErrors' => 0,
        'encryptionKey' => '26bf5675cc6ba691cc55f7ceb397fc80b489dd069ae8ab84f1b07f1768e4cd61f0f1fba58b7fffaef799eb7a0bde143b',
        'exceptionalErrors' => 4096,
        'sitename' => 'Sutsche',
        'systemLogLevel' => 2,
    ],
];
