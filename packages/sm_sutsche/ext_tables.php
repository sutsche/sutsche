<?php
if (! defined('TYPO3_MODE')) {
    die ('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Sutsche');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('pages', 'EXT:sm_sutsche/Resources/Private/Language/locallang_csh_pages.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('pages');


$tempColumns = [

    'pagecolor' => [
        'exclude' => 0,
        'label'   => 'LLL:EXT:sm_sutsche/Resources/Private/Language/locallang_db.xlf:tx_smsutsche_domain_model_pageextend.pagetype',
        'config'  => [
            'type'       => 'select',
            'renderType' => 'selectSingle',
            'items'      => [
                '0' => [

                    '0' => 'default',
                    '1' => '',
                ],
                '1' => [
                    '1' => '-alternate',
                    '0' => 'green',
                ],
            ],
        ],

    ],
];


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns("pages", $tempColumns, 1);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes("pages", "pagecolor;;;;1-1-1");


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages_language_overlay', $tempColumns, 1);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes("pages_language_overlay", "pagecolor;;;;1-1-1");


$tempColumns = [

    'header_icon' => [
        'exclude' => 0,
        'label'   => 'LLL:EXT:sm_sutsche/Resources/Private/Language/locallang_db.xlf:tx_smsutsche_domain_model_pageextend.header_icon',
        'config'  => [
            'type'       => 'select',
            'renderType' => 'selectSingle',
            'items'      => [
                '0'  => [

                    '0' => 'default',
                    '1' => '',
                ],
                '1'  => [
                    '0' => 'Heart',
                    '1' => 'icon--heart',
                ],
                '2'  => [
                    '0' => 'Cloud',
                    '1' => 'icon--cloud',
                ],
                '3'  => [
                    '0' => 'Star',
                    '1' => 'icon--star',
                ],
                '4'  => [
                    '0' => 'Tv',
                    '1' => 'icon--tv',
                ],
                '5'  => [
                    '0' => 'Sound',
                    '1' => 'icon--sound',
                ],
                '6'  => [
                    '0' => 'Video',
                    '1' => 'icon--video',
                ],
                '7'  => [
                    '0' => 'Trash',
                    '1' => 'icon--trash',
                ],
                '8'  => [
                    '0' => 'User',
                    '1' => 'icon--user',
                ],
                '9'  => [
                    '0' => 'Key',
                    '1' => 'icon--key',
                ],
                '10' => [
                    '0' => 'Search',
                    '1' => 'icon--search',
                ],
                '11' => [
                    '0' => 'Settings',
                    '1' => 'icon--settings',
                ],
                '12' => [
                    '0' => 'Camera',
                    '1' => 'icon--camera',
                ],
                '13' => [
                    '0' => 'Tag',
                    '1' => 'icon--tag',
                ],
                '14' => [
                    '0' => 'Lock',
                    '1' => 'icon--lock',
                ],
                '15' => [
                    '0' => 'Bulb',
                    '1' => 'icon--bulb',
                ],
                '16' => [
                    '0' => 'Pen',
                    '1' => 'icon--pen',
                ],
                '17' => [
                    '0' => 'Diamond',
                    '1' => 'icon--diamond',
                ],
                '18' => [
                    '0' => 'Display',
                    '1' => 'icon--display',
                ],
                '19' => [
                    '0' => 'Location',
                    '1' => 'icon--location',
                ],
                '20' => [
                    '0' => 'Eye',
                    '1' => 'icon--eye',
                ],
                '21' => [
                    '0' => 'Bubble',
                    '1' => 'icon--bubble',
                ],
                '22' => [
                    '0' => 'Stack',
                    '1' => 'icon--stack',
                ],
                '23' => [
                    '0' => 'Cup',
                    '1' => 'icon--cup',
                ],
                '24' => [
                    '0' => 'Phone',
                    '1' => 'icon--phone',
                ],
                '25' => [
                    '0' => 'News',
                    '1' => 'icon--news',
                ],
                '26' => [
                    '0' => 'Mail',
                    '1' => 'icon--mail',
                ],
                '27' => [
                    '0' => 'Like',
                    '1' => 'icon--like',
                ],
                '28' => [
                    '0' => 'Photo',
                    '1' => 'icon--photo',
                ],
                '29' => [
                    '0' => 'Note',
                    '1' => 'icon--note',
                ],
                '30' => [
                    '0' => 'Clock',
                    '1' => 'icon--clock',
                ],
                '31' => [
                    '0' => 'Paperplane',
                    '1' => 'icon--paperplane',
                ],
                '32' => [
                    '0' => 'Params',
                    '1' => 'icon--params',
                ],
                '33' => [
                    '0' => 'Banknote',
                    '1' => 'icon--banknote',
                ],
                '34' => [
                    '0' => 'Data',
                    '1' => 'icon--data',
                ],
                '35' => [
                    '0' => 'Music',
                    '1' => 'icon--music',
                ],
                '36' => [
                    '0' => 'Megaphone',
                    '1' => 'icon--megaphone',
                ],
                '37' => [
                    '0' => 'Study',
                    '1' => 'icon--study',
                ],
                '38' => [
                    '0' => 'Lab',
                    '1' => 'icon--lab',
                ],
                '39' => [
                    '0' => 'Food',
                    '1' => 'icon--food',
                ],
                '40' => [
                    '0' => 'T-Shirt',
                    '1' => 'icon--t-shirt',
                ],
                '41' => [
                    '0' => 'Fire',
                    '1' => 'icon--fire',
                ],
                '42' => [
                    '0' => 'clip',
                    '1' => 'icon--clip',
                ],
                '43' => [
                    '0' => 'Shop',
                    '1' => 'icon--shop',
                ],
                '44' => [
                    '0' => 'Calendar',
                    '1' => 'icon--calendar',
                ],
                '45' => [
                    '0' => 'Vallet',
                    '1' => 'icon--vallet',
                ],
                '46' => [
                    '0' => 'Vynil',
                    '1' => 'icon--vynil',
                ],
                '47' => [
                    '0' => 'Truck',
                    '1' => 'icon--truck',
                ],
                '48' => [
                    '0' => 'World',
                    '1' => 'icon--world',
                ],

            ],
        ],

    ],
];


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns("tt_content", $tempColumns, 1);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes("tt_content", "header_icon;Icon", "header", "after:header_layout");

