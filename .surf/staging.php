<?php


$deploymentName = 'Sutsche Staging Website';
$repositoryUrl = 'ssh://git@git.meier.tech:2222/TYPO3/sutsche.git';
$deploymentPath = '/is/htdocs/wp11027778_4NOH7VPDM1/www/staging.sutsche.com';
$context = 'Development';
$composerCommandPath = 'composer';
$hostname = 'staging.sutsche.com';
$username = 'wp11027778';
$phpPath = '/usr/local/bin/php';


$workflow = new \TYPO3\Surf\Domain\Model\SimpleWorkflow();

$application = new \TYPO3\Surf\Application\TYPO3\CMS($deploymentName);


$node = new \TYPO3\Surf\Domain\Model\Node($hostname);
$node->setHostname($hostname);
$node->setOption('username', $username);
$node->setOption('composerCommandPath', $composerCommandPath);

$application->setDeploymentPath($deploymentPath);
$application->setOption('repositoryUrl', $repositoryUrl);
$application->setOption('webDirectory', 'web');
$application->setOption('context', $context);
$application->setOption('phpBinaryPathAndFilename', $phpPath);
$application->setOption('databaseCompareMode', '*.add');
$application->setOption('keepReleases', 4);
$application->setOption(
    'rsyncExcludes',
    [
        '.surf',
        '.ddev',
        '.DS_Store',
        '/.editorconfig',
        '/.git',
        '/.gitignore',
        '/Build',
        '/build.xml',
        '/composer.json',
        '/composer.lock',
        '/web/.htaccess',

    ]
);

$application->setSymlinks(
    [
        'web/typo3conf/AdditionalConfiguration.php' => '../../../../shared/Configuration/AdditionalConfiguration.php',
        'web/.htaccess'                             => '../../../shared/_htaccess',

    ]
);
$application->addNode($node);

$deployment->addApplication($application);
